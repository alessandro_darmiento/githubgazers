package io.alex.githubgazers.io;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by Alex on 10/01/2018.
 */

public class User {
    public static final String TAG = "io.user";

    private String uId;
    private String username;
    private String propicUrl;
    private String userUrl;

    public User(JSONObject userJSON){
        try{
            this.uId = userJSON.getString("id");
            this.username = userJSON.getString("login");
            this.propicUrl = userJSON.getString("avatar_url");
            this.userUrl = userJSON.getString("html_url");
        }catch (Exception e){
            Log.wtf(TAG, e);
        }
    }

    public String getUsername() {
        return username;
    }

    public String getPropicUrl() {
        return propicUrl;
    }

    public String getUserUrl() {
        return userUrl;
    }

    public String getuId() {
        return uId;
    }
}
