package io.alex.githubgazers.network;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by alex on 28/11/16.
 */

public interface VolleyCallback {

    /**
     * @param result
     * @return true if end with success, false otherwise
     */
    boolean onSuccess(JSONArray result);
}