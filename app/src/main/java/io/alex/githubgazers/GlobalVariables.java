package io.alex.githubgazers;

/**
 * Created by Alex on 10/01/2018.
 */

public class GlobalVariables {
    public static final String STARGAZERS_READY = "stargazersready";
    public static final String PAGE_SCROLLER_BOTTOM = "pagescrollerbottom";
    public static final int CHUNK_SIZE = 30;

    public final static int MAJOR = 1;
    public final static int MINOR = 0;
    public final static String AUTHOR = "Alessandro D'Armiento";
}
