package io.alex.githubgazers.gui.adapter;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import io.alex.githubgazers.Core;
import io.alex.githubgazers.R;
import io.alex.githubgazers.io.User;
import io.alex.githubgazers.network.VolleyCallback;
import io.alex.githubgazers.utils.OnItemClickListener;

import static io.alex.githubgazers.GlobalVariables.CHUNK_SIZE;

/**
 * Created by Alex on 10/01/2018.
 * Manages the RecyclerView in MainActivity.
 * Handle chunks download
 */

public class GazersAdapter extends RecyclerView.Adapter<GazersAdapter.ViewHolder> {
    public static final String TAG = "gui.adapter.gazersadptr";

    private List<User> dataset;

    private int nextPage;
    private String user, repo;
    private boolean listIsOver;
    private boolean isDownloading;
    private OnItemClickListener listener;

    public GazersAdapter(OnItemClickListener listener){
        listIsOver = false;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View myView= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_card,parent,false);
        return new ViewHolder(myView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //Log.d(TAG,"onBindViewHolder: " + position);
        User user = dataset.get(position);
        holder.profilePic.setImageURI(Uri.parse(user.getPropicUrl()));
        holder.usernameTextView.setText(user.getUsername());
        holder.userUrlTextView.setText(user.getUserUrl());

        holder.bind(position, user.getUserUrl(), listener); //associate user position with click listener. This is required to match the user click action to the specific card
    }

    @Override
    public int getItemCount() {
        if(null == dataset){
            dataset = new ArrayList<>();
        }
        //Log.d(TAG, "Item Count: " + dataset.size());
        return dataset.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView usernameTextView;
        TextView userUrlTextView;
        SimpleDraweeView profilePic;

        public ViewHolder(View itemView) {
            super(itemView);
            profilePic = (SimpleDraweeView) itemView.findViewById(R.id.card_profile_pic);
            usernameTextView = (TextView) itemView.findViewById(R.id.card_username);
            userUrlTextView = (TextView) itemView.findViewById(R.id.card_url);
        }

        /**
         * Creates a unique association between position in list and item displayed. Used by the
         * onItemClickListener to perform click actions
         * @param position
         * @param userUrl
         * @param listener
         */
        private void bind(final int position,final String userUrl, final OnItemClickListener listener) {
            userUrlTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Log.d(TAG, "onClick. position: " + position);
                    listener.onItemClick(userUrl);
                }
            });
        }

    }


    private boolean update(JSONArray response){
        //Log.d(TAG, "onResponse. Lenght: " + response.length());
        boolean exitCode = true;
        int offset;
        if(nextPage==1){ //offset needs to be set manually if list is empty because getItemCount() can return inconsistent values
            offset = 0;
        } else {
            offset = getItemCount();
        }

        for(int i = 0; i<response.length();i++){
            try{
                dataset.add(new User(response.getJSONObject(i)));
                notifyItemInserted(i + offset);
            } catch (Exception e){
                Log.wtf(TAG, e);
                exitCode = false;
            }
        }

        if(response.length() < CHUNK_SIZE){ //If response is less than a full chunk I can infer the list is over
            Log.d(TAG, "List is over!");
            listIsOver = true;
        }
        return exitCode;
    }


    /**
     * This method start a background download of more data.
     * This is required in order to download users a piece at a time and enhance performance and scalability.
     */
    public void updateFromRemote(){
        isDownloading = true;
        Core.getInstance().getStargazers(user, repo, nextPage, new VolleyCallback() {
            @Override
            public boolean onSuccess(JSONArray result) {
                boolean exitCode = update(result);
                isDownloading = false;
                nextPage++;

                return exitCode;
            }
        });
    }

    /**
     * Reset the adapter.
     * Clean from all previous existing data.
     */
    public void reset(String user, String repo){
        dataset.clear();
        notifyDataSetChanged();
        this.repo = repo;
        this.user = user;
        this.nextPage = 1;
        this.listIsOver = false;
    }

    /**
     * If true, there's no need to call updateFromRemote anymore
     */
    public boolean isListOver(){
        return listIsOver;
    }

    /**
     * If true, there's no need to call updateFromRemote until the current background operation is completed
     */
    public boolean isDownloading(){
        return isDownloading;
    }

}
