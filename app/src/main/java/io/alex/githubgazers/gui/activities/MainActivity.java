package io.alex.githubgazers.gui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import io.alex.githubgazers.Core;
import io.alex.githubgazers.GlobalVariables;
import io.alex.githubgazers.R;
import io.alex.githubgazers.gui.adapter.GazersAdapter;
import io.alex.githubgazers.utils.OnItemClickListener;


public class MainActivity extends AppCompatActivity {
    public static final String TAG = "gui.activities.main";

    private Button startButton;
    private EditText userEditText;
    private EditText repoEditText;
    private RecyclerView gazersContainer;
    private TextView authorVersionTextView;

    private GazersAdapter gazersAdapter;
    private final MainActivity mInstance = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Core.getInstance(this); //Create Core singleton->Initialize core data structures
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE); //Prevent the keyboad to affect the layout

        userEditText = (EditText) findViewById(R.id.main_username);
        repoEditText = (EditText) findViewById(R.id.main_repo);
        startButton = (Button) findViewById(R.id.main_start);
        gazersContainer = (RecyclerView) findViewById(R.id.main_recycler);
        authorVersionTextView = (TextView) findViewById(R.id.author_version);

        authorVersionTextView.setText(GlobalVariables.AUTHOR + " - " + GlobalVariables.MAJOR + "." + GlobalVariables.MINOR);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startIsPressed(userEditText.getText().toString(), repoEditText.getText().toString());
            }
        });


        gazersContainer.setHasFixedSize(true);
        gazersContainer.setLayoutManager(new LinearLayoutManager(this));
        gazersAdapter = new GazersAdapter(  new OnItemClickListener() {
            @Override
            public void onItemClick(String s) { //This function is defined here because adapter should not access the context
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(s));
                startActivity(i);
            }
        });
        gazersContainer.setAdapter(gazersAdapter);

        //LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(GlobalVariables.STARGAZERS_READY));
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(GlobalVariables.PAGE_SCROLLER_BOTTOM));

        gazersContainer.addOnScrollListener(new RecyclerView.OnScrollListener() { //To check when the user reach the bottom of the current downloaded list
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                Log.d(TAG, "onScrolled");
                int lastPosition = ((LinearLayoutManager)recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                Log.d(TAG, "lastPosition: " + lastPosition);
                Log.d(TAG, "GetCount: " + recyclerView.getAdapter().getItemCount());

                if((lastPosition >= recyclerView.getAdapter().getItemCount() - 2) && ! gazersAdapter.isListOver() && ! gazersAdapter.isDownloading()){ //Do not ask for completion if list is already over or if is int the middle of download
                    Log.d(TAG, "Last position reached: " + lastPosition);
                    Toast.makeText(mInstance, "Downloading more results", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(GlobalVariables.PAGE_SCROLLER_BOTTOM);
                    LocalBroadcastManager.getInstance(mInstance).sendBroadcast(intent);
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver); //prevent memory leaks
        super.onDestroy();
    }

    public void startIsPressed(String user, String repo){
        Log.d(TAG, "start button pressed");

        //Notify user if input is invalid
        if(null == user || user.length() == 0 || null == repo || repo.length() == 0){
            Vibrator vibrator =  (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(100);
            return;
        }

        //Set up a new research
        gazersAdapter.reset(user, repo);
        gazersAdapter.notifyDataSetChanged();

        loadGazers();
    }

    private void loadGazers(){
        gazersAdapter.updateFromRemote();
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Received intent " + intent.getAction());

            if(intent.getAction().equals(GlobalVariables.PAGE_SCROLLER_BOTTOM)) { //Load more stargazers
                if(!gazersAdapter.isListOver())
                    loadGazers();
            }
        }

    };



}
