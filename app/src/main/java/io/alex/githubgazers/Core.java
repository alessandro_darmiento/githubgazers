package io.alex.githubgazers;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.drawee.backends.pipeline.Fresco;

import org.json.JSONArray;

import io.alex.githubgazers.network.VolleyCallback;

/**
 * Created by Alex on 10/01/2018.
 * This singleton contains core data structures and methods
 */

public class Core {
    public static final String TAG = "core";

    private static final String BASE_SERVER= "https://api.github.com/";
    private static final String REPOS_EP = "repos/";
    private static final String STARGAZERS_FD = "stargazers";
    private static final String PAGE_FD = "page";

    private static Core mInstance=null;

    private Context ctx;
    private RequestQueue mRequestQueue; //API handler


    private Core(Context context){ //Init stuff goes here
        mRequestQueue = Volley.newRequestQueue(context);
        Fresco.initialize(context); //Required to show Fresco layout elements
        this.ctx = context;
        mInstance = this;
    }

    public synchronized static Core getInstance(@Nullable Context ctx){
        if(null == mInstance){
            if(null == ctx){
                Exception e = new NullPointerException("Trying to create core singleton without context");
                Log.wtf(TAG, e);
            }
            mInstance = new Core(ctx);
        }
        return mInstance;
    }

    public synchronized static Core getInstance(){
        return getInstance(null);
    }

    /**
     * Execute API call to retrieve star gazers list of a given username - repository pair.
     * Response will asynchronously handled by the given callback function
     * @param page chunk identifier
     * @param callback function which will parse the response
     */
    public void getStargazers(String username, String repo, int page, final VolleyCallback callback){
        Log.d(TAG, "getStargazers for user " + username + " and repo" + repo);
        final String getUrl = makeUrl(username, repo, page);

        Log.d(TAG, "Url string: " + getUrl);
        final JsonArrayRequest jsObjRequest = new JsonArrayRequest
                (Request.Method.GET, getUrl, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, "getStargazers: " +  response.toString());
                        if(callback.onSuccess(response)){
                            Log.d(TAG, "Callback onSuccess executed");
                        } else {
                            Exception e = new Exception("Cannot parse json response!!");
                                Log.wtf(TAG, e);
                        }
                        Intent starGazersReady = new Intent(GlobalVariables.STARGAZERS_READY);
                        LocalBroadcastManager.getInstance(getCtx()).sendBroadcast(starGazersReady);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.wtf(TAG,error);
                        Toast.makeText(getCtx(), "No results found..", Toast.LENGTH_SHORT).show();
                    }
                });
        mRequestQueue.add(jsObjRequest);
    }

    static String makeUrl(String username, String repo, int page){
        return BASE_SERVER.concat(REPOS_EP).concat(username).concat("/").concat(repo).concat("/").concat(STARGAZERS_FD).concat("?").concat(PAGE_FD).concat("=").concat(String.valueOf(page));
    }


    public Context getCtx() {
        return ctx;
    }
}
