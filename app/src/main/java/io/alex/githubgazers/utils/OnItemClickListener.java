package io.alex.githubgazers.utils;


/**
 * Created by Alex on 29/11/2016.
 * This interface is used by RecyclerView Adapters to implement an itemClickLister like ListView
 * without having to pass the entire fragment to the adapter constructor
 */

public interface OnItemClickListener {
    void onItemClick(String s);
}