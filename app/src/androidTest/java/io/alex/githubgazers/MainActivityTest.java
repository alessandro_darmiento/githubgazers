package io.alex.githubgazers;

import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.ViewAssertion;
import android.support.test.runner.AndroidJUnit4;

import org.junit.runner.RunWith;

import android.support.test.rule.ActivityTestRule;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.junit.Rule;
import org.junit.Test;

import io.alex.githubgazers.gui.activities.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;

import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;


@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void ensureTextChangesWork() {
        // Type text and then press the button.
        onView(withId(R.id.main_username))
                .perform(typeText("Alessandro"), closeSoftKeyboard());
        onView(withId(R.id.main_repo))
                .perform(typeText("MyRepo"), closeSoftKeyboard());

        //onView(withId(R.id.main_start)).perform(click());

        // Check that the text was changed.
        onView(withId(R.id.main_username)).check(matches(withText("Alessandro")));
        onView(withId(R.id.main_repo)).check(matches(withText("MyRepo")));

    }

    @Test
    public void ensureSingleContentWork() {
        // Type text and then press the button.
        onView(withId(R.id.main_username))
                .perform(typeText("Noblex1991"), closeSoftKeyboard());
        onView(withId(R.id.main_repo))
                .perform(typeText("GGJ16"), closeSoftKeyboard());

        onView(withId(R.id.main_start)).perform(click());

        try {
            Thread.sleep(3000); //Porkaround
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.main_recycler)).check(new RecyclerViewItemCountAssertion(1));
    }

    @Test
    public void ensureFullPageWork() {
        // Type text and then press the button.
        onView(withId(R.id.main_username))
                .perform(typeText("x64dbg"), closeSoftKeyboard());
        onView(withId(R.id.main_repo))
                .perform(typeText("x64dbg"), closeSoftKeyboard());  //With thousands of stars, I can be sure this query returns at least a full page

        onView(withId(R.id.main_start)).perform(click());

        try {
            Thread.sleep(3000); //Porkaround
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.main_recycler)).check(new RecyclerViewItemCountAssertion(GlobalVariables.CHUNK_SIZE));
    }

    class RecyclerViewItemCountAssertion implements ViewAssertion {
        private final int expectedCount;

        public RecyclerViewItemCountAssertion(int expectedCount) {
            this.expectedCount = expectedCount;
        }

        @Override
        public void check(View view, NoMatchingViewException noViewFoundException) {
            if (noViewFoundException != null) {
                throw noViewFoundException;
            }

            RecyclerView recyclerView = (RecyclerView) view;
            RecyclerView.Adapter adapter = recyclerView.getAdapter();
            assertThat(adapter.getItemCount(), is(expectedCount));
        }
    }


     class IdlingHandler implements IdlingResource { //TODO: implement asyncronous testing
        private ResourceCallback resourceCallback;
        private boolean isIdle;

        @Override
        public String getName() {
            return IdlingHandler.class.getName();
        }

        @Override
        public boolean isIdleNow() {
            /*
            if (isIdle) return true;
            if (getCurrentActivity() == null) return false;

            DialogFragment f = (DialogFragment) getCurrentActivity().
                    getFragmentManager().findFragmentByTag(LoadingDialog.TAG);

            isIdle = f == null;
            if (isIdle) {
                resourceCallback.onTransitionToIdle();
            }
            return isIdle;
            */
            return true;
        }
        /*
        public Activity getCurrentActivity() {
            return ( InstrumentationRegistry.
                    getTargetContext().getApplicationContext()).getCurrentActivity();
        }
        */
        @Override
        public void registerIdleTransitionCallback(
                ResourceCallback resourceCallback) {
            this.resourceCallback = resourceCallback;
        }
    }

}