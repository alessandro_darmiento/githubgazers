package io.alex.githubgazers;

import android.support.test.rule.ActivityTestRule;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Rule;
import org.junit.Test;

import io.alex.githubgazers.gui.activities.MainActivity;
import io.alex.githubgazers.network.VolleyCallback;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by Alex on 11/01/2018.
 * This is the first time I write a JUnit Test :P
 */

public class APIValidator  {

    //I control this repository so I can be sure the answer doesn't change
    private final String APIResponseExample0 = "[ { \"login\": \"Noblex1991\", \"id\": 13642937, \"avatar_url\": \"https://avatars1.githubusercontent.com/u/13642937?v=4\", \"gravatar_id\": \"\", \"url\": \"https://api.github.com/users/Noblex1991\", \"html_url\": \"https://github.com/Noblex1991\", \"followers_url\": \"https://api.github.com/users/Noblex1991/followers\", \"following_url\": \"https://api.github.com/users/Noblex1991/following{/other_user}\", \"gists_url\": \"https://api.github.com/users/Noblex1991/gists{/gist_id}\", \"starred_url\": \"https://api.github.com/users/Noblex1991/starred{/owner}{/repo}\", \"subscriptions_url\": \"https://api.github.com/users/Noblex1991/subscriptions\", \"organizations_url\": \"https://api.github.com/users/Noblex1991/orgs\", \"repos_url\": \"https://api.github.com/users/Noblex1991/repos\", \"events_url\": \"https://api.github.com/users/Noblex1991/events{/privacy}\", \"received_events_url\": \"https://api.github.com/users/Noblex1991/received_events\", \"type\": \"User\", \"site_admin\": false } ]";

    private final String exAPI0 = "https://api.github.com/repos/Noblex1991/GGJ16/stargazers?page=1";
    private final String exAPI1 = "https://api.github.com/repos/vertcoin/One-Click-Miner/stargazers?page=2";
    private final String exUser0 = "Noblex1991";
    private final String exRepo0 = "GGJ16";
    private final String exUser1 = "vertcoin";
    private final String exRepo1 = "One-Click-Miner";
    private final int exPage0 = 1;
    private final int exPage1 = 2;


    private VolleyCallback exVolleyCallback0 = new VolleyCallback() {
        @Override
        public boolean onSuccess(JSONArray result) {
            try {
                assertThat(new JSONArray(APIResponseExample0).equals(result), is(true));
                return true;
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }
    };



    @Test
    public void apiStringValidator(){
        String api0 = Core.makeUrl(exUser0, exRepo0, exPage0);
        String api1= Core.makeUrl(exUser1, exRepo1, exPage1);
        assertThat(api0.equals(exAPI0), is(true));
        assertThat(api1.equals(exAPI1), is(true));
    }

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class); //Generate the Context for the API call
    @Test
    public void apiCallValidator(){
        Core.getInstance().getStargazers(exUser0, exRepo0, exPage0, exVolleyCallback0);
    }



}
