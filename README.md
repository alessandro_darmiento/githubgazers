# GitHub Gazers

A simple Android application to display GitHub Star Gazers of a given Username - Repository pair.

## Getting Started

To use the application, simply checkout a new Android Studio project from version control and download from this repository address: https://alessandro_darmiento@bitbucket.org/alessandro_darmiento/githubgazers.git

### Prerequisites

GitHub Gazers uses third party open source library such as Facebook Fresco and Google Volley. All libraries are managed by Gradle. Just make sure you have the latest Gradle version.

To run the application on your phone, ADB driver is also required.


### Installing

Once synchronized with the repository, plug your phone and activate USB Debug option from the developer's settings (enabling this men� could require different steps according to your phone and your current Android version).

Click on the green arrow icon in Android Studio and select your phone to install and lunch the application.

## Usage

```
A well known repository, with lot of Star Gazers, is x64dbg, a Windows debugger.
To inspect x64dbg Star Gazers, insert:
Username: x64dbg
Repository: x64dbg
```
![alt text](https://s9.postimg.org/cusj61nn3/Screenshot_20180111-110746.png)
```
Press start. 
For each entry, Username, Profile pic and clickable link to user profile is displayed.
```
![alt text](https://s9.postimg.org/p9fb6ea0f/Screenshot_20180111-110802.png)
```
Stargazers will be downloaded in chunks of max 30 users at a time. 
Scrolling to the bottom of the page will trigger the download of another chunk of users, until the end of the list is reached.
```
![alt text](https://s9.postimg.org/x25yyd89r/Screenshot_20180111-110819.png)
```
Pressing the start button again will trigger a new execution and discard all previous results. 

```

## Versioning

We use [BitBucket](http://bitbucket.org/) for versioning. 

## Authors

* **Alessandro D'Armiento** - *Initial work* - 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc

